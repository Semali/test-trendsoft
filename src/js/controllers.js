'use strict';

var taskApp = angular.module('taskApp', ['ui.bootstrap', 'angular.filter']);

taskApp.controller('TaskCtrl', ['$scope', '$http', function($scope, $http) {
  $scope.filteredTasks = [],
  $scope.currentPage = 1,
  $scope.numPerPage = 20,
  $scope.maxSize = 5,
  $scope.sortType = ''; // значение сортировки по умолчанию
  $scope.sortReverse = false; // обратная сортировка
  $http.get('src.json').success(function(data) {
    $scope.tasks = data;

    $scope.$watch('currentPage + numPerPage', function() {
      var begin = (($scope.currentPage - 1) * $scope.numPerPage),
      end = begin + $scope.numPerPage;
      
      $scope.filteredTasks = $scope.tasks.slice(begin, end);
    });

    $scope.showMore = function(int) {
      $scope.numPerPage = int;
      $( '.btn' ).removeClass("active");
      if ($scope.numPerPage == $scope.tasks.length) {
        $( '.paging' ).hide();
      } else {
        $( '.paging' ).show();
      }
    }
  });
}]);

taskApp.filter('replaceCircle', function () {
  return function (p) {
      return p == '1' ? p.replace('1', 'orange') : p == '2' ? p.replace('2', 'green') : p.replace('3', 'gray_light');
  };
});